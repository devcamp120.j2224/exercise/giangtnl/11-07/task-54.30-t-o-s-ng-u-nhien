package com.devcamp.s50.task5430.restapi;

import org.springframework.web.bind.annotation.GetMapping;

public class CRandomNumber {
    @GetMapping("/devcamp-welcome/random-double")
    public String randomDoubleNumber(){
        double randomDoubleNum = 0;
        for (int i =1; i<100; i++) {
            randomDoubleNum = Math.random() *100;
        }
        return "Random value in double from 1 to 100: " + randomDoubleNum;
    }
    @GetMapping("/devcamp-welcome/random-int")
    public String randomIntNumber(){
        int randomIntNum = 1 + (int)(Math.random()*((10-1)+1));
        return "Random value in int from 1 to 10: " + randomIntNum;
    }
}
